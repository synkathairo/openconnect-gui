### Snapshot builds

If you are brave and you would like to try upcoming development snapshots
you can check the generated artifacts from recent builds:

 - https://gitlab.com/openconnect/openconnect-gui/-/pipelines

These are unofficial builds, and the artifacts expire weekly please don't complain... ;-)

